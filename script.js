(function() {
    //1 COMPLETE

    function isArrayLengthEqual(array1, array2) {
        return array1.length === array2.length;
    }

    console.log(isArrayLengthEqual(['sad', 'asd'], ['sfgh', 'wqe', 'wesda']));

    //2 COMPLETE

    function filterFalsyValues(array) {
        return array.filter(function(item){
            return  item ;
        });
    }

    var result11 = filterFalsyValues([5, 'asd', false, true, undefined, NaN, null, 'true']);
    console.log(result11);

    //3 COMPLETE

    var getLetterAmountInString = function (array, letter) {
        var splitArray = array.split("");
        return splitArray.reduce(function (result, item) {
            if (item === letter) {
                result++;
            }
            return result;
        }, 0);
    }
    var result3 = getLetterAmountInString('aaasadaaaa', 'a');
    console.log(result3);
})();